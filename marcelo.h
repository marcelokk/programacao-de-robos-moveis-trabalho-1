#ifndef MARCELO_H
#define MARCELO_H

#include <stdio.h>
#include <libplayerc/playerc.h>

#define RAIO 0.30
#define ALCANCE_LASER 8

//playerc_client_t* connect();
//playerc_position2d_t* subscribe_position();
//void shutdown(playerc_client_t *client, playerc_position2d_t *position);

// Create a client and connect it to the server.
playerc_client_t* mconnect() {
  playerc_client_t *client = playerc_client_create(NULL, "localhost", 6665);
  if(0 != playerc_client_connect(client)) {
    printf("ERRO - connect\n");
    exit(0);
  }
  return client;
}

// Create and subscribe to a position2d device.
playerc_position2d_t* subscribe_position(playerc_client_t *client) {
  playerc_position2d_t *position2d = playerc_position2d_create(client, 0);
  if(playerc_position2d_subscribe(position2d, PLAYER_OPEN_MODE)) {
    printf("ERRO - subscribe_position\n");
    exit(0);
  }
  return position2d;
}

playerc_laser_t* subscribe_laser(playerc_client_t *client) {
    playerc_laser_t *laser = playerc_laser_create(client, 0);
    if(playerc_laser_subscribe(laser, PLAYERC_OPEN_MODE)) {
        printf("ERRO - subscribe_laser\n");
        exit(0);
    }
    return laser;
}

// Shutdown
void mshutdown(playerc_client_t *client, playerc_position2d_t *position) {
  playerc_position2d_unsubscribe(position);
  playerc_position2d_destroy(position);
  playerc_client_disconnect(client);
  playerc_client_destroy(client);
}

#endif
