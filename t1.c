#include <stdio.h>
#include <libplayerc/playerc.h>
#include "marcelo.h"
#include <assert.h>

#define FOLGA 0.3			// distancia minima entre o robo e o checkpoint para ele ter "passado" pelo checkpoint
#define RAD_TO_GRAUS 57.39	// constante de conversao de radianos para graus

#define SPEED 0.3

// playerc_position2d_set_cmd_vel(position2d, speed, 0, DTOR(turn), 1);

void alinha(playerc_client_t *client, playerc_position2d_t *position, double graus) {
	playerc_position2d_set_cmd_vel(position, 0, 0, DTOR(0), 1);
	double diferenca = graus - position->pa*RAD_TO_GRAUS;
	while(0.3 < diferenca || diferenca < -0.3) {
	    playerc_client_read(client);
		playerc_position2d_set_cmd_vel(position, 0, 0, DTOR(diferenca), 1);
		diferenca = graus - position->pa*RAD_TO_GRAUS;
	}
	playerc_position2d_set_cmd_vel(position, 0, 0, DTOR(0), 1);
}

void esquerda(playerc_client_t *client, playerc_position2d_t *position) {
	alinha(client, position, 180);
	playerc_position2d_set_cmd_vel(position, SPEED, 0, DTOR(0), 1);
}

void direita(playerc_client_t *client, playerc_position2d_t *position) {
	alinha(client, position, 0);
	playerc_position2d_set_cmd_vel(position, SPEED, 0, DTOR(0), 1);
}

void sobe(playerc_client_t *client, playerc_position2d_t *position) {
	alinha(client, position, 90);
	playerc_position2d_set_cmd_vel(position, SPEED, 0, DTOR(0), 1);
}

void desce(playerc_client_t *client, playerc_position2d_t *position) {
	alinha(client, position, -90);
	playerc_position2d_set_cmd_vel(position, SPEED, 0, DTOR(0), 1);
}

void para(playerc_client_t *client, playerc_position2d_t *position) {
	playerc_position2d_set_cmd_vel(position, 0, 0, DTOR(0), 1);
}

int main(int argc, char *argv[]) {
	if(argc < 2) {
		printf("ERRO - numero de argumentos invalido\n");
		printf("Uso: ./%s [nome do arquivo com o path]\n", argv[0]);
		return 1;
	}

    playerc_client_t *client = mconnect();
    playerc_position2d_t *position = subscribe_position(client);
    playerc_laser_t *laser = subscribe_laser(client);

	FILE *path = fopen(argv[1], "r");
	FILE *log = fopen("log.txt", "w");
	assert(path != NULL && log != NULL);

	int i;
	for(i = 0; i < 5; i++)
	    playerc_client_read(client);

	int checkpoint = 0;
	double x, y;
	double rx, ry;
	while(1) {
		fscanf(path, "%lf%lf", &x, &y);

		if(x == 9999 && y == 9999)
			break;
		printf("procurando pelo checkpoint %d: x %lf y %lf\n", checkpoint, x, y);
		checkpoint++;
//*
		while(1) {
			playerc_client_read(client);
			rx = position->px;
			ry = position->py;
			//printf("position2d : %f %f %f\n", position->px, position->py, position->pa*57.29);

/*
			switch(getchar()) {
				case 'w':
					printf("Sobe\n");
					sobe(client, position);
					break;
				case 'a':
					printf("Esquerda\n");
					esquerda(client, position);
					break;
				case 's':
					printf("Desce\n");
					desce(client, position);
					break;
				case 'd':
					printf("Direita\n");
					direita(client, position);
					break;
				default:
					break;
			}
//*/
			if(x - FOLGA < rx && rx < x + FOLGA)
				if(y - FOLGA < ry && ry < y + FOLGA)
					break;

			if(rx > x + FOLGA) {
#ifdef DEBUG_MOVIMENTOS
				printf("esquerda: x%lf y%lf\n", rx, ry);
#endif
				esquerda(client, position);
			}
			else if(x - FOLGA > rx) {
#ifdef DEBUG_MOVIMENTOS
				printf("direita: x%lf y%lf\n", rx, ry);
#endif
				direita(client, position);
			}
			else if(ry > y + FOLGA) {
#ifdef DEBUG_MOVIMENTOS
				printf("desce: x%lf y%lf\n", rx, ry);
#endif
				desce(client, position);
			}
			else if(y - FOLGA > ry) {
#ifdef DEBUG_MOVIMENTOS
				printf("sobe: x%lf y%lf\n", rx, ry);
#endif
				sobe(client, position);
			}

		}
//*/
	}
	printf("passou por todos os checkpoints!\n");
    mshutdown(client, position);
	fclose(path);
	fclose(log);
	return 0;
}











/*
		    for(i=0; i<180; i++) {
		        if(laser->scan[i][0] < minR) 
		            minR = laser->scan[i][0];
		        if(laser->scan[i+180][0] < minL) 
		            minL = laser->scan[i+180][0];
		    }
*/
